import Map from 'ol/Map.js';
import View from 'ol/View.js';
import Feature from 'ol/Feature';
import { Draw, Modify, Snap } from 'ol/interaction.js';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer.js';
import { OSM, Vector as VectorSource } from 'ol/source.js';
import { Stroke, Style } from 'ol/style.js';
import { LineString } from 'ol/geom.js';
import GeoJSON from 'ol/format/GeoJSON';
import * as random from "./utils/random-strings.js"
import * as calculus from "./utils/geometric-calculus.js"
import Overlay from 'ol/Overlay.js';

let draw, snap;
let helpTooltipElement;
let helpTooltip;
let measureTooltip;
let measureTooltipElement;
let sketch;

let pointerMoveHandler = (eventPointer)=> {  
  if (eventPointer.dragging) {
    return
  }
  let helpMsg = 'Click to start drawing';
  if (sketch) {
    var geom = (sketch.getGeometry());
    let continueLineMsg = "Click to continue drawing the line";
    if (geom instanceof LineString) {
      helpMsg = continueLineMsg;
    }
  }
  helpTooltipElement.innerHTML = helpMsg;
  helpTooltip.setPosition(eventPointer.coordinate);
  helpTooltipElement.classList.remove('hidden');
}

const getCoords = document.querySelector('.getCoord')
const exportJSON = document.getElementById('link')

const geojson = new GeoJSON()

var raster = new TileLayer({
  source: new OSM()
});

let featuresObj = {}

var source = new VectorSource();

var vector = new VectorLayer({
  source: source,
  style: new Style({
    stroke: new Stroke({
      color: '#ffcc33',
      width: 2
    })
  })
});

let map = new Map({
  layers: [raster, vector],
  target: 'map',
  view: new View({
    center: [-5483129.009695, -1883881.331059],
    projection: 'EPSG:3857',
    zoom: 15
  })
});

map.on('pointermove', pointerMoveHandler);

map.getViewport().addEventListener('mouseout', function() {
  helpTooltipElement.classList.add('hidden');
});

var modify = new Modify({source: source});

map.addInteraction(modify);

function addInteractions() {
  draw = new Draw({
    source: source,
    type: 'LineString',
    style: new Style({
      stroke: new Stroke({
        color: 'rgba(0, 0, 0, 0.5)',
        lineDash: [10,10],
        width: 2
      })
    })
  });

  map.addInteraction(draw);

    createMeasureToolTip();
    createHelpTooltip()

  snap = new Snap({source: source});

  map.addInteraction(snap);
  
  draw.on('drawstart', function(drawStartEvent){
    sketch = drawStartEvent.feature;
    drawStartEvent.feature.setId(random.generateID())
    let listener = sketch.getGeometry().on('change', function(event){      
      let output;
      let tooltipCoord;
      let geometry = event.target;
      if (geometry instanceof LineString) {        
        output = calculus.formatLength(geometry);        
        tooltipCoord = geometry.getLastCoordinate();        
      }
      measureTooltipElement.innerHTML = output;
      measureTooltip.setPosition(tooltipCoord);
    });
  })
  
  draw.on('drawend', function(drawEndEvent) {
    
    let feature = drawEndEvent.feature;
    feature.setStyle(random.randomStyle())
    
    featuresObj[drawEndEvent.feature.getId()] = geojson.writeFeature(feature);

    measureTooltipElement.className = 'tooltip tooltip-static';
    
    measureTooltip.setOffset([0, -7]);  
    
    sketch = null;    
    
    measureTooltipElement = null;
    
    createMeasureToolTip();

  }, this);

  modify.on('modifystart', function(modifyStartEvent){

  })
  
  modify.on('modifyend', function(modifyEndEvent){
    console.log(featuresObj)
    let features = modifyEndEvent.features;
    features.forEach(function(feature){
      if(featuresObj[feature.getId()]){
        featuresObj[feature.getId()] = geojson.writeFeature(feature)
      }
    })
  })

  var coordinateCluster = {}

  function getCoordsCallback(){
    var points = [
      [-5483386.984665028, -1883663.9632600716],
      [-5483124.232380297, -1883907.6062877306],
      [-5483057.349980548, -1883549.3077176437]
    ]

    var featureLine = new Feature({
      geometry: new LineString(points)
    });

    var sourceLine = new VectorSource({
      features: [featureLine]
    });

    var vectorLine = new VectorLayer({
      source: sourceLine
    });

    map.addLayer(vectorLine);

  }

  function getJSON(){    
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(coordinateCluster));
    exportJSON.setAttribute("href", dataStr);
    exportJSON.setAttribute("download", "LineString");    
  }


  getCoords.addEventListener('click', getCoordsCallback)
  exportJSON.addEventListener('click', getJSON)
}

let createHelpTooltip = ()=> {
  if (helpTooltipElement) {
    helpTooltipElement.parentNode.removeChild(helpTooltipElement);
  }
  helpTooltipElement = document.createElement('div');
  helpTooltipElement.className = 'tooltip hidden';
  helpTooltip = new Overlay({
    element: helpTooltipElement,
    offset: [15, 0],
    positioning: 'center-left'
  });
  map.addOverlay(helpTooltip);
}

let createMeasureToolTip = ()=>{
  
  if (measureTooltipElement) {
    measureTooltipElement.parentNode.removeChild(measureTooltipElement);
  }
  
  measureTooltipElement = document.createElement('div');
  
  measureTooltipElement.className = "tooltip tooltip-measure";
  measureTooltip = new Overlay({
    element: measureTooltipElement,
    offset: [0, -15],
    positioning: 'bottom-center'
  });
  map.addOverlay(measureTooltip);
}

addInteractions();



/*
  1 - Usuário insere no mapa quantas LineStrings ele quiser.
    1.1. Ele pode deletar essa LineString?
      1.1.1. Ele selecionará a LineString que quer deletar e deletará.
      1.1.2. As LineStrings serão salvas em um objeto. Quando ele deletar a LineString específica
      no mapa, automaticamente ela será deletada do objeto.
    1.2. Ele pode modificar essa LineString?
      1.2.1. Ao modificar o LineString o objeto alterado dever ser "persistido" no objeto. "OK"
    1.3. Ele pode salvar essa LineString.
      1.3.1. O usuário pode gerar um JSON do objeto com as LineStrings salvas.
    1.4. Ele pode importar conjunto de pontos de um arquivo JSON
*/


/*
  Fluxo de execução:
  1 - Fonte de dados TileLayer será montada;
  2 - Fonte de dados Vector será montada;
  3 - Camada Layers será montada e um estilo pra ela sera atribuído;
  4 - Map é criado para interações da View e dos Layers;
  5 - 
*/